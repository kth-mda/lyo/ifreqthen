package se.kth.mmk.md;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.rmf.reqif10.AttributeValueString;
import org.eclipse.rmf.reqif10.SpecHierarchy;
import org.eclipse.rmf.reqif10.SpecObject;
import org.eclipse.rmf.reqif10.Specification;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;


public class ReqifResourceTest {

    public static final String REQIF_FILENAME = "my.reqif";

    @Ignore("Not profiling in VisualVM")
//    @BeforeClass
    public static void delay_for_profiler() throws InterruptedException {
        //gimme time to press the profiling button!
        Thread.sleep(8000);

        //warm up
        String filename = getReqifFilename(REQIF_FILENAME);
        ReqIfResource resource = new ReqIfResource(filename);
    }

    private static String getReqifFilename(String file) {
        return ReqifResourceTest.class.getResource("/" + file).getFile();
    }

    @Test
    public void spec_is_notEmpty() {
        String filename = getReqifFilename(REQIF_FILENAME);
        ReqIfResource resource = new ReqIfResource(filename);

        List<Specification> specs = resource.getSpecs();

        assertNotEquals("ReqIf file must at least 1 spec", 0, specs.size());
    }


    @Test
    public void specMetadata_is_notEmpty() {
        String filename = getReqifFilename(REQIF_FILENAME);

        ReqIfResource resource = new ReqIfResource(filename);

        assertTrue("Resource identifier must be set", StringUtils.isNotBlank(resource.getIdentifier()));
    }

    @Test
    public void spec_has_correctItems() {
        String filename = getReqifFilename(REQIF_FILENAME);

        ReqIfResource resource = new ReqIfResource(filename);
        List<Specification> specs = resource.getSpecs();

        assertEquals("ReqIf file has 1 spec", 1, specs.size());
        assertEquals("Spec is called 'Specification Document'", "Specification Document", specs.get(0).getLongName());
    }

    @Test
    public void spec_has_correctRequirements() {
        String filename = getReqifFilename(REQIF_FILENAME);

        ReqIfResource resource = new ReqIfResource(filename);
        List<SpecObject> requirements = resource.getAllRequirements();
        List<String> requirementValues = requirements.stream().map(specObject -> ((AttributeValueString) specObject
                .getValues().get(0)).getTheValue()).collect(Collectors.toList());

        assertEquals("ReqIf file has 3 requirements", 3, requirements.size());
        assertArrayEquals(new String[]{
                "ProR should be usable",
                "ProR should work under Eclipse",
                "ProR should use ReqIf format"}, requirementValues.toArray(new String[3]));
    }


    @Test
    public void specElement_has_refinements() {
        String filename = getReqifFilename(REQIF_FILENAME);
        ReqIfResource resource = new ReqIfResource(filename);

        List<Specification> specs = resource.getSpecs();
        Specification mainSpec = specs.get(0);
        SpecHierarchy specRootElement = mainSpec.getChildren().get(0);

        assertEquals(1, mainSpec.getChildren().size());
        assertEquals(2, specRootElement.getChildren().size());
    }

    @Test
    public void reqifAdapter_matches_source() {
        String filename = getReqifFilename("ProR_Search_Spec.reqif");
        ReqIfResource resource = new ReqIfResource(filename);

        List<ReqifSpecification> specifications = resource.getSpecifications();
        ReqifSpecification firstReqifSpecification = specifications.get(0);
        List<ReqifSpecificationElement> reqifSpecRootElements = firstReqifSpecification.getSpecificationElements();

        assertThat(reqifSpecRootElements.size(), equalTo(5));
    }

    //TODO move to a spec hierarchy
    private void printSpecs(List<Specification> specs) {
        for (Specification spec :
                specs) {
            System.out.printf("SPEC\t'%s'\n", spec.getLongName());
            printSpecHierarchy(spec.getChildren());
        }
    }

    //TODO move to a spec hierarchy
    private void printSpecHierarchy(EList<SpecHierarchy> children) {
        printSpecHierarchy(children, 0);
    }

    private void printSpecHierarchy(EList<SpecHierarchy> children, int level) {
        for (SpecHierarchy hierarchy :
                children) {
            System.out.printf("%s REQ '%s'\n", StringUtils.repeat("\t", level), ((AttributeValueString) hierarchy
                    .getObject()
                    .getValues().get(0)).getTheValue());
            printSpecHierarchy(hierarchy.getChildren(), level + 1);
        }
    }
}
