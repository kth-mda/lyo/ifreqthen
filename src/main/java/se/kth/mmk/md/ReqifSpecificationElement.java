package se.kth.mmk.md;

import org.apache.commons.lang3.text.StrBuilder;
import org.eclipse.emf.common.util.EList;
import org.eclipse.rmf.reqif10.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Adapter class for the ReqIf SpecificationElement
 */
public class ReqifSpecificationElement {
    private final SpecObject _e;
    private final List<ReqifSpecificationElement> children = new ArrayList<>();
    private final List<ReqifAttribute> attributes = new ArrayList<>();
    private String identifier;
    private String desc;
    private GregorianCalendar lastChange;
    public ReqifSpecificationElement(SpecHierarchy hierarchy) {
        _e = hierarchy.getObject();

        buildChildrenAdapters(hierarchy.getChildren());
        buildAttributeAdapters();
        extractMetadata();
    }

    public List<ReqifAttribute> getAttributes() {
        return attributes;
    }

    public List<ReqifSpecificationElement> getChildren() {
        return children;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getDesc() {
        return desc;
    }

    public GregorianCalendar getLastChange() {
        return lastChange;
    }

    public ReqifAttribute getAttribute(String name) {
        return attributes
                .stream()
                .filter(reqifAttribute -> reqifAttribute.getLabel().equals(name))
                .findFirst()
                .get();
    }

    private void extractMetadata() {
        identifier = _e.getIdentifier();
        desc = _e.getDesc();
        lastChange = _e.getLastChange();
    }

    private void buildAttributeAdapters() {
        for (AttributeValue attr :
                _e.getValues()) {
            ReqifAttribute attribute = buildAttributeAdapter(attr);
            attributes.add(attribute);
        }
    }

    private ReqifAttribute buildAttributeAdapter(AttributeValue attr) {
        if (attr instanceof AttributeValueString) {
            return new ReqifAttributeString(attr);
        } else if (attr instanceof AttributeValueInteger) {
            return new ReqifAttributeInteger(attr);
        } else if (attr instanceof AttributeValueXHTML) {
            return new ReqifAttributeXmlLiteral(attr);
        }
        // TODO implement AttributeValueBoolean
        // TODO implement AttributeValueDate
        // TODO implement AttributeValueReal
        // TODO implement AttributeValueEnumeration?
        throw new IllegalArgumentException("Failed to build an AttributeValue adapter");
    }

    private void buildChildrenAdapters(EList<SpecHierarchy> _children) {
        for (SpecHierarchy child :
                _children) {
            children.add(new ReqifSpecificationElement(child));
        }
    }

    public String toStringTree() {
        StrBuilder sb = new StrBuilder("► REQ:");
        sb.append('"')
                .append(_e.getType().getLongName())
                .append('"');
        sb.appendNewLine();
        for (ReqifAttribute attr :
                attributes) {
            sb.appendln(Utils.padLines(attr.toString(), ' ', 4));
        }
        for (ReqifSpecificationElement element :
                children) {
            sb.appendln(Utils.padLines(element.toStringTree(), ' ', 2));
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReqifSpecificationElement{");
        sb.append("identifier='").append(identifier).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
