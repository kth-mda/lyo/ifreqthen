package se.kth.mmk.md;

import de.kay_muench.reqif10.reqifparser.ReqIF10Parser;
import org.eclipse.rmf.reqif10.*;

import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * ReqIfResource represents resources within a single ReqIF file.
 */
public class ReqIfResource {

    private final ReqIFHeader reqifHeader;
    private ReqIF10Parser parser = new ReqIF10Parser();
    private ReqIF resource;
    private ReqIFContent reqifContent;
    private List<ReqifSpecification> specifications = new ArrayList<>();

    public ReqIfResource(String filename) {
        parser.setReqIFFilename(filename);
        resource = parser.parseReqIFContent();
        reqifHeader = resource.getTheHeader();
        reqifContent = resource.getCoreContent();
        buildAdapters();  // TODO be lazy
    }

    public ReqIfResource(InputStream inputStream) {
        parser.setReqifStream(inputStream);
        resource = parser.parseReqIFContent();
        reqifHeader = resource.getTheHeader();
        reqifContent = resource.getCoreContent();
        buildAdapters();  // TODO be lazy
    }

    public List<ReqifSpecification> getSpecifications() {
        return specifications;
    }

    private void buildAdapters() {
        for (Specification spec :
                reqifContent.getSpecifications()) {
            specifications.add(new ReqifSpecification(spec));
        }
    }

    /**
     * Unique identifier for the underlying XML document.
     *
     * @return xsd:ID compatible identifier
     */
    public String getIdentifier() {
        return reqifHeader.getIdentifier();
    }

    /**
     * @return ReqIF resource creation datetime in Java 8 format
     * @see <a href="https://docs.oracle.com/javase/tutorial/datetime/iso/legacy.html">Legacy Date-Time Code</a>
     */
    public ZonedDateTime getCreationDateTime() {
        return reqifHeader.getCreationTime().toZonedDateTime();
    }

    /**
     * Adapter for {@link ReqIFHeader#getRepositoryId()}. Has tool-generated unique ID in the ReqIF 'Repository Id'
     * field that is supposed to point to the repository where requirements where exported from.
     *
     * @return database ID, URL or other identifier of the original requirements repository
     */
    public String getRepositoryId() {
        return reqifHeader.getRepositoryId();
    }

    @Deprecated
    public List<Specification> getSpecs() {
        return reqifContent.getSpecifications();
    }


    /**
     * @return flat list of all requirements in the ReqIF resource
     */
    @Deprecated
    public List<SpecObject> getAllRequirements() {
        return reqifContent.getSpecObjects(); // TODO add wrapper here
    }

}
