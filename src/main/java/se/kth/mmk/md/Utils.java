package se.kth.mmk.md;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;

/**
 * Utils for string manipulation .
 *
 * @author Andrew Berezovskyi <andriib@kth.se>
 * @since 2016-05-11
 */
public class Utils {
    public static String padLines(String str, char pad, int count) {
        String[] lines = str.split("\\R+");  // Java 8+
        StrBuilder sb = new StrBuilder();
        for (String line : lines) {
            sb.append(StringUtils.repeat(pad, count));
            sb.appendln(line);
        }
        return sb.toString();
    }
}
