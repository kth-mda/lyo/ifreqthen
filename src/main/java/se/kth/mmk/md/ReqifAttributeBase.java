package se.kth.mmk.md;

/**
 * ReqifAttributeBase is a base class for the {@link ReqifAttribute} implementations.
 *
 * @author Andrew Berezovskyi <andriib@kth.se>
 * @since 2016-05-11
 */
public abstract class ReqifAttributeBase<T> implements ReqifAttribute<T> {
    @Override
    public String getStringValue() {
        return getValue().toString();
    }

    @Override
    public String toString() {
        return String.format("%s=\"%s\"", getLabel(), getStringValue());
    }
}
