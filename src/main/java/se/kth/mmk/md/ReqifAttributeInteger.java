package se.kth.mmk.md;

import org.eclipse.rmf.reqif10.AttributeValue;
import org.eclipse.rmf.reqif10.AttributeValueInteger;

import java.math.BigInteger;

/**
 * ReqifAttributeInteger is a wrapper around {@link AttributeValueInteger}.
 *
 * @author Andrew Berezovskyi <andriib@kth.se>
 * @since 2016-05-11
 */
public class ReqifAttributeInteger extends ReqifAttributeBase<BigInteger> {
    private final AttributeValueInteger _value;

    public ReqifAttributeInteger(AttributeValue attributeValue) {
        _value = (AttributeValueInteger) attributeValue;
    }

    @Override
    public BigInteger getValue() {
        return _value.getTheValue();
    }

    @Override
    public String getLabel() {
        return _value.getDefinition().getLongName();
    }
}
