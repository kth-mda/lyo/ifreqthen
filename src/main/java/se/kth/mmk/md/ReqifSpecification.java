package se.kth.mmk.md;

import org.apache.commons.lang3.text.StrBuilder;
import org.eclipse.rmf.reqif10.SpecHierarchy;
import org.eclipse.rmf.reqif10.Specification;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Adapter class for the ReqIF Specification
 */
public class ReqifSpecification {
    private final Specification _s;
    private final List<ReqifSpecificationElement> specificationElements = new ArrayList<>();

    public ReqifSpecification(final Specification spec) {
        _s = spec;
        buildAdapters();
    }

    public String getTitle() {
        return _s.getLongName();
    }

    public ZonedDateTime getLastModified() {
        return _s.getLastChange().toZonedDateTime();
    }

    public String getDescription() {
        return _s.getDesc();
    }

    public String getIdentifier() {
        return _s.getIdentifier();
    }

    public List<ReqifSpecificationElement> getSpecificationElements() {
        return specificationElements;
    }

    private void buildAdapters() {
        for (SpecHierarchy hierarchy :
                _s.getChildren()) {
            specificationElements.add(new ReqifSpecificationElement(hierarchy));
        }
    }

    @Override
    public String toString() {
        StrBuilder sb = new StrBuilder("SPEC:");
        sb.append('"')
                .append(getTitle())
                .append('"')
                .appendNewLine();
        for (ReqifSpecificationElement element :
                specificationElements) {
            sb.appendln(Utils.padLines(element.toStringTree(), ' ', 3));
        }
        return sb.toString();
    }
}
