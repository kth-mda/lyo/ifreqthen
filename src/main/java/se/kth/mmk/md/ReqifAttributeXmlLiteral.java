package se.kth.mmk.md;

import org.eclipse.rmf.reqif10.AttributeValue;
import org.eclipse.rmf.reqif10.AttributeValueXHTML;

/**
 * ReqifAttributeXmlLiteral  is a wrapper around {@link AttributeValueXHTML}.
 *
 * @author Andrew Berezovskyi <andriib@kth.se>
 * @since 2016-05-11
 */
public class ReqifAttributeXmlLiteral extends ReqifAttributeBase<String> {


    private final AttributeValueXHTML _value;

    public ReqifAttributeXmlLiteral(AttributeValue attributeValue) {
        _value = (AttributeValueXHTML) attributeValue;
    }

    @Override
    public String getValue() {
        return _value.getTheValue().getXhtmlSource();
    }

    @Override
    public String getLabel() {
        return _value.getDefinition().getLongName();
    }
}
