package se.kth.mmk.md;

import de.kay_muench.reqif10.reqifparser.ReqIF10Parser;

/**
 * Created by andrew on 29.04.16.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Initiating ReqIF10Parser");
        ReqIF10Parser parser = new ReqIF10Parser();
        System.out.printf("Parser %s", System.identityHashCode(parser));
        System.out.println("Shutting down");
    }
}
