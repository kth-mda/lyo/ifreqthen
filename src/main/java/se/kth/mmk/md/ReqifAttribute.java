package se.kth.mmk.md;

/**
 * A more sane {@link org.eclipse.rmf.reqif10.AttributeValue}.
 * Subclasses of {@link org.eclipse.rmf.reqif10.AttributeValueSimple}:
 * <ul>
 * <li>AttributeValueBoolean</li>
 * <li>AttributeValueDate (GregorianCalendar)</li>
 * <li>AttributeValueInteger</li>
 * <li>AttributeValueReal (double)</li>
 * <li>AttributeValueString</li>
 * </ul>
 * <p>
 * AttributeValueEnumeration and AttributeValueXHTML are direct subclasses of
 * {@link org.eclipse.rmf.reqif10.AttributeValue}.
 */
public interface ReqifAttribute<T> {
    public T getValue();
    public String getStringValue();

    public String getLabel();
}
