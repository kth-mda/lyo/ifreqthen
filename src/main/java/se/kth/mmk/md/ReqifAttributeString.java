package se.kth.mmk.md;

import org.eclipse.rmf.reqif10.AttributeValue;
import org.eclipse.rmf.reqif10.AttributeValueString;

/**
 * ReqifAttributeString is a wrapper around {@link AttributeValueString}.
 *
 * @author Andrew Berezovskyi <andriib@kth.se>
 * @since 2016-05-11
 */
public class ReqifAttributeString extends ReqifAttributeBase<String> {

    private final AttributeValueString _value;

    public ReqifAttributeString(AttributeValue attributeValue) {
        _value = (AttributeValueString) attributeValue;
    }

    @Override
    public String getValue() {
        return _value.getTheValue();
    }

    @Override
    public String getLabel() {
        return _value.getDefinition().getLongName();
    }
}
