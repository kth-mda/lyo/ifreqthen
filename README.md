# ifreqthen

## build instructions

To build this project, we need to build [`reqiftools`](https://gitlab.com/kth-mda/lyo/reqif-parser).

It needs 2 other Eclipse projects to build:

1. `org.eclipse.rmf@6b78ea7`
2. `org.eclipse.sphinx@12926f1`

### `org.eclipse.rmf`

    cd org.eclipse.rmf.releng/
    mvn clean install -P platform-mars -Dmaven.test.skip=true

### `org.eclipse.sphinx`

    cd releng/org.eclipse.sphinx.releng.builds && mvn clean install -P platform-mars -Dmaven.test.skip=true
    cd plugins/org.eclipse.sphinx.emf && mvn clean install -P platform-mars -Dmaven.test.skip=true
    cd plugins/org.eclipse.sphinx.emf.serialization && mvn clean install -P platform-mars -Dmaven.test.skip=true


## License

> Copyright 2023 KTH Royal Institute of Technology
> 
> Licensed under the EUPL-1.2-or-later.
>
> SPDX-License-Identifier: EUPL-1.2

This project builds upon the fork of a [reqif-parser](https://gitlab.com/kth-mda/lyo/reqif-parser) project, with the following license:

> Copyright (c) 2023 KTH Royal Institute of Technology
> Copyright (c) 2014 Kay Erik Münch.
> 
> All rights reserved. This program and the accompanying materials
> are made available under the terms of the Eclipse Public License v1.0
> which accompanies this distribution, and is available at
> http://www.spdx.org/licenses/EPL-1.0
>
> SPDX-License-Identifier: EPL-1.0